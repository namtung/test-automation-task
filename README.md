## Running the test app

This is a relatively simple Next.js app written in TypeScript. To run it you should,

1. Install dependencies with `yarn`
2. Start the dev server with `yarn dev`

API routes have been used to simulate the presence of a backend, the code there is not even close to production-ready but should suffice for this task.

There is a small degree of persistence in the app data. To reset the app data you can do one of the following,

-   GET `localhost:3000/api/reset`
-   Click the `Reset` button in the top-right of the running app

The valid login credentials for the app are,

1. Username: "admin", password: "adminpass" for admin user
2. Username: 'user', password: "helloworld" for normal user

## Functional requirements

To help you plan your tests, here are some functional requirements for the app,

1.  Users can log in by providing a user name and password
2.  When not logged-in, only the login page can be visited by a user
3.  Logged-in users are categorised as "normal users" or "admin users"
4.  All logged-in users are able to see a list of cats on the home page
    1. This list includes a name, picture, awesomeness rating and rank (in order of descending awesomeness) for each cat
    2. Awesomeness is calculated as the sum of the ASCII character codes for the letters of the cat's name (cats only user ASCII characters in their names). However if the cat's name is exactly "James", the awesomeness is infinite.
    3. The cats are presented in descending order of awesomeness.
5.  Only admin users are able to delete cats from the list.
6.  All logged-in users may rename cats but 2 cats may never share a name (cats are not picky about their exact names but desire to feel unique)
7.  Any changes to the cat list are persisted between visits to the app.

## Attribution

All cat images are used without modification from https://placekitten.com/attribution.html.

## Test plan and implementation
The test plan cover two subjects for the app:
-  API testing (implemented with [Postman](https://learning.postman.com/docs/running-collections/using-newman-cli/command-line-integration-with-newman/))
-  GUI or end-to-end testing (implemented with [Nightwatch.js](https://nightwatchjs.org/))

Testing is automated with GitlabCI and triggered when new commit is pushed.
https://gitlab.com/namtung/test-automation-task/-/pipelines

Test environment is dockerized in image `namtung/test-automation-env:stable` and deployed in my slave computer.
## API testing
There are two main test suite implemented at `test-cases/API-test-cases/`
Two suite have identical test cases, but the latter suite is executed without auth token and expected 401 response (unauthorized)

In `API-test-cases-authorized.json` test suite:
-   GET `localhost:3000/api/kitties`: Test verify if response status is 200 and response body contains valid key-value
-   GET `localhost:3000/api/reset`: Test verify if response status is 200 
-   DELETE `localhost:3000/api/James`: Test verify if response status is 200
-   PUT `localhost:3000/api/James`: Test verify if response status is 200

In `API-test-cases-unauthorized.json` test suite:
-   GET `localhost:3000/api/kitties`: Test verify if response status is 401
-   GET `localhost:3000/api/reset`: Test verify if response status is 401
-   DELETE `localhost:3000/api/James`: Test verify if response status is 401
-   PUT `localhost:3000/api/James`: Test verify if response status is 401

To run the test:
-   `yarn install`
-   `newman run test-cases/API-test-cases/API-test-cases-authorized.json`

Or you can import the .json scripts to Postman and execute them.

## GUI testing
There are 4 test suite implemented at `test-cases/GUI-test-cases/`:

-  In `TestAuthentication.js` test suite: 
    1.  Test if login page is redirected page when not logged-in
    2.  Test valid login with `admin` and `adminpass`
    3.  Test valid login with `user` and `helloworld`
    4.  Test invalid login with wrong username/password
    5.  Test invalid login with empty username
    5.  Test invalid login with empty password
- In `TestRename.js` test suite:
    1. Test rename kitty James to Tung
    2. Test duplicate when rename kitty James to Dups
    3. Test rename kitty James to Dups
- In `TestDelete.js` test suite:
    1. Test admin can see delete button and delete kitty James
    2. Test user cannot see delete button
- In `TestListOrder.js` test suite:
    1. Test if James awesomeness is equal to Infinity
    2. Test if awesomeness is sorted in descending order

Common functions to interact with page object is written at `test-cases/page-objectes/homepage.js`

To run the test:
-   `yarn install`
-   `npx nightwatch --env chrome`

## Other information
Pipeline configuration is written at `.gitlab-ci.yml`
Pipeline steps are fairly simple:
1.  Build docker image `test-automation-app`
2.  Deploy the image to test server
3.  Run API test cases
4.  Run E2E test cases

Docker test environment image is written at `test-cases/test-env/Dockerfile`
Docker React application image is written at `Dockerfile`

## Key features missing and room for improvement:
-   HTML report is not created at end of the test, thus, summary of test results is not easily visible and accessible anywhere
    -   Possible solution: utilize Nightwatch add-on  `nightwatch-html-reporter`
-   Test case to validate list includes name, picture, awesomeness rating and rank for each cat is missing
-   Test case to validate any changes to the cat list are persisted between visits to the app is missing
-   GUI testing is not supported in Firefox, Safari and Edge browsers yet
-   Code quality is fairly low, as there were many hard-coded variables in test scripts