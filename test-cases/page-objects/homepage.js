var pageCommands = {
    login: function(username, password){
        return this
        .setValue('#username', username)
        .setValue('#password', password)
        .click('button[type=submit]')
    },
    reset: function(){
        return this.click('#reset-btn')
    },
    logout: function(){
        return this.click('#loginout-btn')
    },    
    rename: function(newName){
        this
            .clearValue('#edit-btn')
            .setValue('#edit-btn', newName)
            .click('#save-btn')
        this.api.refresh()
        return this
    },
    delete: function(){
        return this.click('#delete-btn')
    },
    listElements: function(){
        let kittyList = []
        this.getText('#kitty-list', (response) => { 
                let = elementList = response.value.split("\n")
                try {
                    for(var i=0; i < elementList.length; i=i+5){
                        kittyList.push({
                            rank: elementList[i+1],
                            awesomeness: elementList[i+3],
                            name: elementList[i+4]
                        })
                    }
                } catch (error){
                    console.log(error)
                }
            })

        return kittyList
    }
}

module.exports = {
    url: 'http://localhost:3000/',
    commands: [pageCommands],
    elements: {
        submit: 'button[type=submit]',
    }
}