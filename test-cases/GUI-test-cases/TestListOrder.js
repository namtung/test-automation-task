module.exports = {
    beforeEach: function (browser) {
        browser.page.homepage()
            .navigate()
            .login(browser.globals.admin.username, browser.globals.admin.password)
        },

    'Test if  James awesomeness is Infinite and awesomeness is sorted with descending order': function (browser) {
        let kittyList = browser.page.homepage().listElements()
        browser.perform(()=>{
            for(var i=0; i<kittyList.length-1; i++){
                if(kittyList[i].name == "James") {
                    browser.assert.equal(kittyList[i].awesomeness, '∞', 'James awesomeness is strictly ∞')
                    kittyList[i].awesomeness = Infinity
                }
            }
        })
        browser.perform(()=>{
            for(var i=0; i<kittyList.length-1; i++){
                browser.assert.ok(kittyList[i].awesomeness > kittyList[i+1].awesomeness,  `${kittyList[i].name} is ranked higher than ${kittyList[i+1].name}`)
            }
        })
    },

    afterEach: function (browser) {
        browser.page.homepage()
            .reset()
            .end();
    }
}