module.exports = {
    beforeEach: function (browser) {
        browser.page.homepage().navigate();
    },
    'Test admin can see delete button and delete kitty James': function (browser) {
        browser.page.homepage().login(browser.globals.admin.username, browser.globals.admin.password)
        browser.page.homepage()
            .assert.elementPresent("#delete-btn")
            .delete()
            .assert.not.containsText('#kitty-list', 'James')
    },
    'Test user cannot see delete button': function (browser) {
        browser.page.homepage().login(browser.globals.user.username, browser.globals.user.username)
        browser.page.homepage()
            .assert.not.elementPresent("#delete-btn")
    },
    afterEach: function (browser) {
        browser.page.homepage()
            .reset()
            .end();
    }
}