module.exports = {
  beforeEach: function (browser) {
    browser.page.homepage().navigate();
  },  
  'Test login page when not logged-in': function (browser) {
    browser
      .assert.urlEquals('http://localhost:3000/login')
  },  
  'Test valid login as admin': function (browser) {
    browser.page.homepage()
      .login(browser.globals.admin.username, browser.globals.admin.password)
      .assert.urlEquals('http://localhost:3000/')
  },
  'Test valid login as user': function (browser) {
    browser.page.homepage()
    .login(browser.globals.user.username, browser.globals.user.password)
    .assert.urlEquals('http://localhost:3000/')
  },   
  'Test invalid login with incorrect username/password': function (browser) {
    browser.page.homepage()
      .login(browser.globals.admin.username, browser.globals.user.password)
      .assert.not.urlEquals('http://localhost:3000/')
  },
  'Test invalid login with empty password': function (browser) {
    browser.page.homepage()
    .login(browser.globals.admin.username, "")
    .assert.not.urlEquals('http://localhost:3000/')
  },
  'Test invalid login with empty username': function (browser) {
    browser.page.homepage()
    .login("", browser.globals.user.password)
    .assert.not.urlEquals('http://localhost:3000/')
  },
  
  afterEach: function (browser) {
    browser.page.homepage().end();
  }   
};