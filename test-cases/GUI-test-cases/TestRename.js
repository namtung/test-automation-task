module.exports = {
    beforeEach: function (browser) {
        browser.page.homepage().navigate();
        browser.page.homepage().login(browser.globals.admin.username, browser.globals.admin.password)
    },

    'Test rename kitty James to Tung': function (browser) {
        browser.page.homepage()
            .rename('Tung')
            .assert.containsText('#kitty-list', 'Tung')
    },
    'Test duplicate kitty James to Dups': function (browser) {
        browser.page.homepage()
            .rename('Dups')
            .assert.containsText('#kitty-list', 'James')
    },
    'Test rename kitty James to empty string': function (browser) {
        browser.page.homepage()
            .rename('')
            .assert.containsText('#kitty-list', 'James')
    },
    afterEach: function (browser) {
        browser.page.homepage()
            .reset()
            .end();
    }
}